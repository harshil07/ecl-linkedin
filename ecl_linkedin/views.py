import logging

from django.http import HttpResponseRedirect
from django.views.decorators.http import require_GET

import constants
import linkedin

from decorators import linkedin_callback

logger = logging.getLogger(__name__)

@require_GET
def oauth_linkedin_begin(request):
    client = linkedin.LinkedIn(commas=True)
    data = client.generate_authorization_url()
    token = data['oauth_token']
    secret = data['oauth_token_secret']

    request.session['temporary_oauth_token'] = token
    request.session['temporary_oauth_secret'] = secret
    url = linkedin.LINKEDIN_BASE_URL + linkedin.LINKEDIN_OAUTH_AUTHENTICATE
    return HttpResponseRedirect(url + '?oauth_token=' + token)

@require_GET
@linkedin_callback
def oauth_linkedin_complete(request, data):
    return HttpResponseRedirect(constants.LINKEDIN_REDIRECT_URL)

